package main

import (
	"fmt"
	"github.com/hyperjumptech/grule-rule-engine/ast"
	"github.com/hyperjumptech/grule-rule-engine/builder"
	"github.com/hyperjumptech/grule-rule-engine/engine"
	"github.com/hyperjumptech/grule-rule-engine/pkg"
	"os"
	"swym/internal"
)

func main() {
	tweetEventState := &internal.TweetEventState{
		Username: "",
		HashTag:  "",
		Location: "",
		Count:    0,
	}

	if err := tweetEventState.SetAction("special_emoji", func() error {
		fmt.Println("WOOHOOO 😀")
		return nil
	}); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	dataCtx := ast.NewDataContext()
	err := dataCtx.Add("TweetEventState", tweetEventState)
	if err != nil {
		panic(err)
	}
	knowledgeLibrary := ast.NewKnowledgeLibrary()
	ruleBuilder := builder.NewRuleBuilder(knowledgeLibrary)
	drls := `
rule CheckValues "Check the default values" salience 10 {
    when 
        TweetEventState.Count == 4
    then
        TweetEventState.Do("special_emoji");
        Retract("CheckValues");
		Complete();
}
`
	bs := pkg.NewBytesResource([]byte(drls))
	err = ruleBuilder.BuildRuleFromResource("TutorialRules", "0.0.1", bs)
	if err != nil {
		panic(err)
	}
	knowledgeBase := knowledgeLibrary.NewKnowledgeBaseInstance("TutorialRules", "0.0.1")
	eng := engine.NewGruleEngine()
	for !dataCtx.IsComplete() {
		err = eng.Execute(dataCtx, knowledgeBase)
		if err != nil {
			panic(err)
		}
		dataCtx.Reset()
		tweetEventState.Count += 1
	}
}
