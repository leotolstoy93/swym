## Dependencies

github.com/hyperjumptech/grule-rule-engine

## Motivation

- Initially I tried thinking about building my own solution, and then I realized halfway that it would require building
a rule engine of some sort.
- I figured there must be some library out there. Upon searching, I stumbled upon an article about rule engines by
Martin Fowler that led me understand how rule engines work - at least on a high level.
- I found the Go library mentioned in the *Dependencies* section, and figured that it would be a good fit for building
a basic solution.

## Approach

- Any *Event* type struct has to implement two methods - SetAction (with a name and a function of type func() error) and
Do(with the name of the action function).
- The rules can be set in a template - in code, in JSON files or any other appropriate format.
- The set rules would get executed once a certain criterion is met and the appropriate action function would get called.

## Comments

- This code along with the research took me around 2 hours to write.
- It just gives a high level idea that the POC works.