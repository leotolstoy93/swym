package internal

import (
	"fmt"
	"sync"
)

var once sync.Once

type BaseEvent struct {
	actionNameToFunc map[string]func() error
}

func NewBaseEvent() *BaseEvent {
	be := BaseEvent{}
	be.actionNameToFunc = make(map[string]func() error)
	return &be
}

func (b *BaseEvent) IsActionPresent(name string) bool {
	if _, ok := b.actionNameToFunc[name]; ok {
		return true
	}
	return false
}

func (b *BaseEvent) SetActionFunc(name string, action func() error) {
	b.actionNameToFunc[name] = action
}

func (b *BaseEvent) GetActionFunc(name string) (func() error, error) {
	actionFunc, ok := b.actionNameToFunc[name]
	if !ok {
		return nil, fmt.Errorf("%s: action func not found", name)
	}
	return actionFunc, nil
}
