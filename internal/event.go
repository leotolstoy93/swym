package internal

import (
	"errors"
	"fmt"
)

type TweetEventState struct {
	*BaseEvent
	Username string `json:"username"`
	HashTag  string `json:"hash_tag"`
	Location string `json:"location"`
	Count    int64  `json:"count"`
}

func (t *TweetEventState) SetAction(actionName string, actionFunc func() error) error {
	once.Do(func() {
		fmt.Println("initializing base event for TweetEventState...")
		t.BaseEvent = NewBaseEvent()
		fmt.Println("base event initialized...")
	})
	if t.BaseEvent.IsActionPresent(actionName) {
		return fmt.Errorf("%s: already exists. please choose another action name", actionName)
	}
	t.BaseEvent.SetActionFunc(actionName, actionFunc)
	return nil
}

func (t *TweetEventState) Do(actionName string) error {
	if t.BaseEvent == nil {
		return errors.New("base event has not been initialized yet")
	}
	if !t.IsActionPresent(actionName) {
		return fmt.Errorf("%s: invalid action", actionName)
	}
	actionFunc, _ := t.GetActionFunc(actionName)
	return actionFunc()
}
